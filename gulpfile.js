// Gulp
var gulp = require('gulp');
var runSequence = require('run-sequence'); // https://www.npmjs.com/package/run-sequence (temporary until gulp 4.0 comes out)
var del = require('del'); // https://github.com/gulpjs/gulp/blob/master/docs/recipes/delete-files-folder.md
// Sass module
var sass = require('gulp-sass');
var postcss = require('gulp-postcss'); // https://github.com/postcss/gulp-postcss
// Typescript module
var ts = require('gulp-typescript'); // dependency : npm install typescript
var sourcemaps = require('gulp-sourcemaps');
// Pug module
var pug = require('gulp-pug'); // https://www.npmjs.com/package/gulp-pug
// Minifiers
var imagemin = require('gulp-imagemin'); // https://www.npmjs.com/package/gulp-imagemin 
var minifier = require('gulp-minifier'); //https://www.npmjs.com/package/gulp-minifier

// Location variables
var locations = {
    src: "src",
    dest: "dest"
}

gulp.task('build', function (callback) {
    runSequence('clean', ['sass', 'pug', 'images', 'ts', 'dts', 'copy'], 'watch', callback);
});
// CLEAN
gulp.task('clean', function () {
    return del([locations.dest + '/*']);
});
// COPY
gulp.task('copy', function () {
    // return gulp
    gulp.src(locations.src + '/fonts/*').pipe(gulp.dest(locations.dest + '/static/fonts'));
    gulp.src(locations.src + '/uploads/**').pipe(gulp.dest(locations.dest + '/uploads'));
    gulp.src(locations.src + '/js/**').pipe(gulp.dest(locations.dest + '/static/js'));
    gulp.src(locations.src + '/css/external/*').pipe(gulp.dest(locations.dest + '/static/style'));
}

);
// Images Task
gulp.task('images', () =>
    gulp.src(locations.src + '/img/*')
        .pipe(imagemin([
            imagemin.gifsicle({ interlaced: true }),
            imagemin.jpegtran({ progressive: true }),
            imagemin.optipng({ optimizationLevel: 5 }),
        ]))
        .pipe(gulp.dest(locations.dest + '/static/img'))
);
// Style Task ( SASS )
gulp.task('sass', function () {
    return gulp.src(locations.src + '/css/main.scss').pipe(sass()).pipe(minifier({
        minify: true,
        minifyCSS: true,
        collapseWhitespace: true
    })).pipe(gulp.dest(locations.dest + '/static/style'))
}

);
// JS Task ( TYPESCRIPT )
gulp.task('ts', function () {
    return gulp.src(locations.src + '/ts/**.ts').pipe(ts({
        noImplicitAny: true,
        out: 'main.js'
    }))
        .js.pipe(sourcemaps.write())
        .pipe(minifier({
            minify: true,
            minifyJS: true,
            collapseWhitespace: true
        }))
        .pipe(gulp.dest(locations.dest + '/static/js'));
}

);
// TS Task ( TYPESCRIPT Definition files )
gulp.task('dts', function () {
    return gulp.src([locations.src + '/js/**/*.ts', '!' + locations.src + '/js/external/**/*.ts']).pipe(ts({
        declaration: true
    })).dts.pipe(gulp.dest(locations.dest + '/static/js/d.ts'));
}

);
// HTML Task ( PUG )
gulp.task('pug', function () {
    var variables = {};
    return gulp.src(locations.src + '/html/*.pug').pipe(pug({
        pretty: false
    })).pipe(gulp.dest(locations.dest)) // Move HTML files to the root folder
}

);
// Watch task
gulp.task('watch', function () {
    gulp.watch(locations.src + '/css/**/*.scss', ['sass']);
    gulp.watch(locations.src + '/ts/**/*.ts', ['ts', 'dts']);
    gulp.watch(locations.src + '/html/**/*.pug', ['pug']);
    gulp.watch(locations.src + '/img/*', ['images']);
    gulp.watch(locations.src + '/fonts/*', ['copy']);
    gulp.watch(locations.src + '/uploads/**', ['copy']);
    gulp.watch(locations.src + '/js/*.js', ['copy']);
}

);