# Readme

Readme file for Arthesian's Portfolio

## First run

1. Make sure NPM ( Node Package Manager ) is installed. You can verify if NPM is installed by running the following command:
    > npm -v

    If it returns a value, NPM is correctly installed, else you need to install NPM first

2. Open Command Prompt, and go to the project location
    > cd %project-location%

3. Install all the Gulp packages required for running this project
    1. Install all the packages
        > npm install

4. You should now be able to run and build the project by running the following command:
    > gulp build ( or ctrl + shift + b )

    The project will build everytime changes are detected in the source files ( .ts, .scss, .jade )

## Usage

This project's purpose is to have a site dedicated to myself :D