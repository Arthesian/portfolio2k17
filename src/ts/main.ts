/// <reference path="external/jquery/index.d.ts" />
/// <reference path="external/createjs-lib/index.d.ts" />
/// <reference path="external/createjs/index.d.ts" />
/// <reference path="external/easeljs/index.d.ts" />
/// <reference path="external/soundjs/index.d.ts" />
/// <reference path="external/preloadjs/index.d.ts" />
/// <reference path="external/tweenjs/index.d.ts" />
/// <reference path="external/pixi.js/index.d.ts" />

/// <reference path="modules/helpers.ts" />
/// <reference path="modules/engine.ts" />
/// <reference path="modules/basevisual.ts" />
/// <reference path="modules/visualizer.ts" />
/// <reference path="modules/visual_bubblestream.ts" />

/// <reference path="modules/scrollProgressBar.ts" />
/// <reference path="modules/scrollNavigationActive.ts" />
/// <reference path="modules/scrollButton.ts" />
/// <reference path="modules/smoothScroll.ts" />

/**
 * The page code that initializes all the fun stuff! :D
 */

var VISUALIZER = new Visualizer();
var BUBBLES = new BubbleStream(VISUALIZER);

VISUALIZER.Engine.generateFakeAudioData();