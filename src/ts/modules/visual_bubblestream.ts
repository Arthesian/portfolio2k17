class BubbleStream extends BaseVisual {

    private topSinBubbles: any[] = [];
    private topCosBubbles: any[] = [];
    private botSinBubbles: any[] = [];
    private botCosBubbles: any[] = [];

    public settings = {
        bubblesSpeed: 100,
        bubblesColor: +"0x0066FF",
        bubblesAmount: 50
    }

    constructor(visualizer: Visualizer) {
        super(visualizer);
        this.Container.name = "bubblestream container";

        this.create();
    }

    public reset() {
        this.botCosBubbles = [];
        this.botSinBubbles = [];
        this.topCosBubbles = [];
        this.topSinBubbles = [];

        this.clear();
        this.create();
    }

    public create() {
        for (var x = 0; x < this.settings.bubblesAmount * 3; x++) {

            var circle = new BubbleCircle();
            circle.beginFill(this.settings.bubblesColor);
            circle.drawCircle(0, 0, 20);
            //var img = this.Engine.getLoadedImageSprite("witch");

            var circle = circle;

            circle.progress = Math.random() * 12 - 1;

            var r_scale = Math.random() + 0.5; // 0.5 - 1.5;
            circle.scale.set(r_scale, r_scale);
            circle.origScale = r_scale;

            var r_speed = Math.random() + 0.5; // 0.5 - 1.5;
            circle.speed = r_speed;
            circle.origSpeed = r_speed;

            var r_offset = Math.random() * 0.4 + 0.8; // 0.8 - 1.2;
            circle.offset = r_offset / r_scale;
            circle.origOffset = r_offset / r_scale;

            var r_alpha = Math.random() * 0.4 + 0.1;
            circle.alpha = r_alpha;
            circle.alpha = r_alpha;

            this.Container.addChild(circle);
            this.topSinBubbles.push(circle);
        }

        for (var x = 0; x < this.settings.bubblesAmount * 3; x++) {

            var circle = new BubbleCircle();
            circle.beginFill(this.settings.bubblesColor);
            circle.drawCircle(0, 0, 20);
            //var img = this.Engine.getLoadedImageSprite("witch");

            var circle = circle;

            circle.progress = Math.random() * 12 - 1;

            var r_scale = Math.random() + 0.5; // 0.5 - 1.5;
            circle.scale.set(r_scale, r_scale);
            circle.origScale = r_scale;

            var r_speed = Math.random() + 0.5; // 0.5 - 1.5;
            circle.speed = r_speed;
            circle.origSpeed = r_speed;

            var r_offset = Math.random() * 0.4 + 0.8; // 0.8 - 1.2;
            circle.offset = r_offset / r_scale;
            circle.origOffset = r_offset / r_scale;

            var r_alpha = Math.random() * 0.4 + 0.1;
            circle.alpha = r_alpha;
            circle.alpha = r_alpha;

            this.Container.addChild(circle);
            this.topCosBubbles.push(circle);
        }

        for (var x = 0; x < this.settings.bubblesAmount * 0.4; x++) {

            var circle = new BubbleCircle();
            circle.beginFill(this.settings.bubblesColor);
            circle.drawCircle(0, 0, 50);
            //var img = this.Engine.getLoadedImageSprite("witch");

            var circle = circle;

            circle.progress = Math.random() * 12 - 1;

            var r_scale = Math.random() * 3 + 0.5; // 0.5 - 3.5;
            circle.scale.set(r_scale, r_scale);
            circle.origScale = r_scale;

            var r_speed = Math.random() + 0.5; // 0.5 - 1.5;
            circle.speed = r_speed;
            circle.origSpeed = r_speed;

            var r_offset = Math.random() * 0.4 + 0.8; // 0.8 - 1.2;
            circle.offset = r_offset / r_scale;
            circle.origOffset = r_offset / r_scale;

            var r_alpha = Math.random() * 0.2 + 0.01;
            circle.alpha = r_alpha;
            circle.alpha = r_alpha;

            this.Container.addChild(circle);
            this.botSinBubbles.push(circle);
        }

        for (var x = 0; x < this.settings.bubblesAmount * 0.4; x++) {

            var circle = new BubbleCircle();
            circle.beginFill(this.settings.bubblesColor);
            circle.drawCircle(0, 0, 50);
            //var img = this.Engine.getLoadedImageSprite("witch");

            var circle = circle;

            circle.progress = Math.random() * 12 - 1;

            var r_scale = Math.random() * 3 + 0.5; // 0.5 - 3.5;
            circle.scale.set(r_scale, r_scale);
            circle.origScale = r_scale;

            var r_speed = Math.random() + 0.5; // 0.5 - 1.5;
            circle.speed = r_speed;
            circle.origSpeed = r_speed;

            var r_offset = Math.random() * 0.4 + 0.8; // 0.8 - 1.2;
            circle.offset = r_offset / r_scale;
            circle.origOffset = r_offset / r_scale;

            var r_alpha = Math.random() * 0.2 + 0.01;
            circle.alpha = r_alpha;
            circle.alpha = r_alpha;

            this.Container.addChild(circle);
            this.botCosBubbles.push(circle);
        }
    }

    public update() {

        var tenthWidth = (this.Visualizer.Engine.Canvas.width / 10);
        var tenthHeight = (this.Visualizer.Engine.Canvas.height / 5);
        var halfHeight = this.Visualizer.Engine.Canvas.height / 2;

        var bassModifier = this.SoundData.slice(0, 6).average();
        bassModifier /= 2;
        bassModifier += 0.5;

        var midModifier = this.SoundData.slice(20, 50).average();
        midModifier /= 2;
        midModifier += 0.5;

        this.topSinBubbles.forEach(object => {
            object.progress += object.speed * 0.01 * (this.settings.bubblesSpeed / 100);

            if (object.progress > 11) {
                object.progress = -1;
            }

            object.x = object.progress * tenthWidth;
            object.y = Math.sin(object.progress) * tenthHeight + halfHeight / 1.25;
            object.y *= object.offset;
            object.scale.set(object.origScale * midModifier);
        });

        this.topCosBubbles.forEach(object => {
            object.progress += object.speed * 0.01 * (this.settings.bubblesSpeed / 100);

            if (object.progress > 11) {
                object.progress = -1;
            }

            object.x = object.progress * tenthWidth;
            object.y = Math.cos(object.progress) * object.offset * tenthHeight + halfHeight / 1.25;
            object.y *= object.offset;
            object.scale.set(object.origScale * midModifier);
        });

        this.botSinBubbles.forEach(object => {
            object.progress += object.speed * 0.01 * (this.settings.bubblesSpeed / 100);

            if (object.progress > 11) {
                object.progress = -1;
            }

            object.x = object.progress * tenthWidth;
            object.y = Math.sin(object.progress) * tenthHeight + halfHeight / 2;
            object.scale.set(object.origScale * bassModifier);
        });

        this.botCosBubbles.forEach(object => {
            object.progress += object.speed * 0.01 * (this.settings.bubblesSpeed / 100);

            if (object.progress > 11) {
                object.progress = -1;
            }

            object.x = object.progress * tenthWidth;
            object.y = Math.cos(object.progress) * tenthHeight + halfHeight * 1.5;
            object.scale.set(object.origScale * bassModifier);
        });
    }
}

class BubbleCircle extends PIXI.Graphics {
    public progress: number;
    public speed: number;
    public origSpeed: number;
    public origScale: number;
    public offset: number;
    public origOffset: number;
}