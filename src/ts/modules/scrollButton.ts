/// <reference path="../external/jquery/index.d.ts" />

class ScrollButton {

    public settings = {
        selector: ".scroll__button",
        context: null,
        switchOffset: 100
    }

    constructor(document) {
        this.settings.context = document;

        this.bind();

        // Kick it off at init
        this.onScroll();
    }

    public bind() {
        $(this.settings.context).on("scroll", () => this.onScroll());
    }

    private onScroll() {
        var scroll = window.scrollY;

        var $button = $(this.settings.selector);

        if (scroll > this.settings.switchOffset) {
            if (!$button.hasClass("up")) {
                $button.addClass("up");
                $button.attr("href", "#main");
            }

        } else {
            if ($button.hasClass("up")) {
                $button.removeClass("up");
                $button.attr("href", "#about-me");
            }

        }
    }
}

var scrollButton = new ScrollButton(document);