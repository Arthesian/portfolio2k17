/// <reference path="../external/jquery/index.d.ts" />

class ScrollProgressBar {

    public settings = {
        selector: ".scrollprogress__bar",
        context: null,
        animationTime: 1000
    }

    constructor(document: Document) {
        this.settings.context = document;

        this.bind();

        $(this.settings.selector).width(0);
    }

    public bind() {
        $(this.settings.context).on("scroll", () => this.onScroll());
    }

    private onScroll() {
        var height = $(this.settings.context).height() - window.innerHeight;
        var scroll = window.scrollY;

        var progress = Math.round((scroll / height) * 100);

        $(this.settings.selector).clearQueue().animate({ width: progress + "%" }, this.settings.animationTime);
    }
}

var scrollProgressBar = new ScrollProgressBar(document);