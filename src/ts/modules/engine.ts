class Engine {

    static Container = PIXI.Container;

    // Properties
    public Renderer = PIXI.autoDetectRenderer(1280, 720, { antialias: false, transparent: false, resolution: 1 });
    public Container = new PIXI.Container();
    public Loader = PIXI.loader;
    public Resources = PIXI.loader.resources;
    public Sprite = PIXI.Sprite;
    public Texture = PIXI.Texture;
    public BaseTexture = PIXI.BaseTexture;
    public Ticker = createjs.Ticker;
    public Shape = PIXI.Graphics;
    public Canvas;

    public OnTick: () => void;

    constructor() {
        this.Canvas = this.Renderer.view;
        $(this.Canvas).addClass("bubble-flow").attr("style", "");

        this.Renderer.autoResize = false;
        this.autoSizeRenderer();

        this.OnTick = () => { console.log("No Update Method defined!"); }

        $("#top-bubble-flow").append(this.Canvas);

        this.Ticker.addEventListener("tick", () => this.OnTick());
        this.setFPS(25);

        this.bind();
    }

    public bind() {
        $(window).on("resize", () => this.autoSizeRenderer());
    }

    public autoSizeRenderer() {
        this.Renderer.resize(window.innerWidth, window.innerHeight);
    }

    public setFPS(fps: number) {
        this.Ticker.setFPS(fps);
    }

    public getLoadedImageSprite(name) {
        var resource = this.Resources[name].texture;

        if (!resource) { console.error("No such texture was loaded!"); return; };

        var sprite = new this.Sprite(resource);
        sprite.anchor.set(0.5, 0.5);
        sprite.position.set(this.Canvas.width / 2, this.Canvas.height / 2);

        return sprite;
    }

    private _currentData: any[] = [];
    public AudioData: any[] = [];

    public generateFakeAudioData() {
        setInterval(() => this._generateFakeAudioData(), 500); //~ 20FPS
    }

    private _generateFakeAudioData() {
        var data = [];
        for (var x = 0; x < 128; x++) {
            data.push(Math.random() * 1.1);
        }

        this.setAudioData(data);
    }

    public setAudioData(data: number[]) {
        if (this.AudioData.length === 0) {
            this.AudioData = data;
        }

        this._currentData = data;

        createjs.Tween.get(this.AudioData, { override: true }).to(this._currentData, 500);
    }

    public update() {
        this.Renderer.render(this.Container);
    }
}
