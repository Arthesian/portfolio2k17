/// <reference path="basevisual.ts" />
/// <reference path="engine.ts" />
/// <reference path="../external/jquery/index.d.ts" />

class Visualizer {

    public Engine: Engine;
    public visuals: IVisual[];
    public Objects: any[] = [];

    public settings: {

    }

    constructor(settings?: any) {

        this.Engine = new Engine();

        this.Engine.OnTick = () => this.update();

        this.settings = $.extend({}, this.settings, settings);

        this.init();
    }

    public init() {
        this.visuals = [];
    }

    public addVisual(visual: IVisual, position?: number) {

        this.Engine.Container.addChild(visual.Container);

        this.visuals.push(visual);
    }

    public removeVisual(visual: IVisual) {

        this.Engine.Container.removeChild(visual.Container);

        var i = this.visuals.indexOf(visual);
        this.visuals.splice(i, 1);
    }

    public setAudioData(data: number[]) {

    }

    private update() {

        this.renderAllVisuals();
    }

    public renderAllVisuals() {

        this.visuals.forEach(visual => {
            visual.setSoundData(this.Engine.AudioData);
            visual.update();
        });

        this.Engine.update();
    }
}