class ScrollNavigationActive {

    public settings = {
        sectionSelecor: ".section",
        navigationLinkSelector: "#navigation > ul > li > a"
    }

    private variables = {
        sections: null,
        sectionLinks: null,
        sectionLinksById: null
    }

    constructor(settings: any) {
        this.settings = $.extend({}, this.settings, settings);

        this.bind();
    }

    public bind() {
        this.variables.sections = $($(this.settings.sectionSelecor).get().reverse());
        this.variables.sectionLinks = $(this.settings.navigationLinkSelector);

        this.variables.sectionLinksById = [];
        this.variables.sections.each((i: number, section: any) => {
            var id = $(section).attr("id");
            this.variables.sectionLinksById[id] = $(this.settings.navigationLinkSelector + '[href="#' + id + '"]');
            this.variables.sectionLinksById.push(this.variables.sectionLinksById[id]);
        });

        this.checkValidity();

        $(window).on("scroll", () => this.onScroll());
    }

    private onScroll() {
        this.highlightNavigation();
    }

    public highlightNavigation() {
        // get the current vertical position of the scroll bar
        var scrollPosition = $(window).scrollTop();

        // iterate the sections
        this.variables.sections.each((i: number, section: any) => {
            var currentSection = $(section);
            // get the position of the section
            var sectionTop = currentSection.offset().top;

            // if the user has scrolled over the top of the section  
            if (scrollPosition >= sectionTop - 10) {
                // get the section id
                var id = currentSection.attr('id');
                // get the corresponding navigation link
                var navigationLink = this.variables.sectionLinksById[id];
                // if the link is not active
                if (!navigationLink.hasClass('active')) {
                    // remove .active class from all the links
                    this.variables.sectionLinks.removeClass('active');
                    // add .active class to the current link
                    navigationLink.addClass('active');
                }
                // we have found our section, so we return false to exit the each loop
                return false;
            }
        });
    }

    private checkValidity() {
        if (!this.variables.sections || this.variables.sections.length === 0) { console.error("No sections found! Please enter a valid selector."); }
        if (!this.variables.sectionLinks || this.variables.sectionLinks.length === 0) { console.error("No menu links found! Please enter a valid selector."); }
        if (!this.variables.sectionLinksById || this.variables.sectionLinksById.length === 0) { console.error("No matches between links and sections found! Please check id's and href attributes"); }
    }
}

var scrollNavigationActive = new ScrollNavigationActive({
    sectionSelecor: "section",
    navigationLinkSelector: "#navbar > ul > li > a"
});