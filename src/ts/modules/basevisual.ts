/// <reference path="../external/pixi.js/index.d.ts" />

/// ------------- VISUALS --------------- ///
class BaseVisual implements IVisual {

    public Visualizer: Visualizer;

    public Container: PIXI.Container;

    public SoundData: number[];

    constructor(visualizer: Visualizer) {
        this.Container = new Engine.Container();
        this.Container.name = "base visual container";

        this.Visualizer = visualizer;
        this.Visualizer.addVisual(this);

        this.SoundData = [];
    }

    public setSoundData(data: number[]) {
        this.SoundData = data;
    }

    public create() {

    }

    public clear() {
        this.Container.removeChildren();
    }

    public update() {

    }
}

interface IVisual {
    Container: PIXI.Container;

    setSoundData(data: number[]);
    clear();
    create();
    update();
}