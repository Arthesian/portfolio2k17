// Helper methods
interface Array<T> {
    sum: () => number;
    take: (number: number) => Array<T>;
    average: () => number;
    interpolate: (newlength: number) => Array<T>;
}


Array.prototype.sum = Array.prototype.sum || function () {
    return this.reduce(function (sum: number, a: any) { return sum + Number(a) }, 0);
}

Array.prototype.take = Array.prototype.take || function (num: number) {
    return this.slice(0, num);
}

Array.prototype.average = Array.prototype.average || function () {
    return this.sum() / (this.length || 1);
}

Array.prototype.interpolate = Array.prototype.interpolate || function (newLength: number) {

    var data = this;

    var linearInterpolate = function (before: any, after: any, atPoint: any) {
        return before + (after - before) * atPoint;
    };

    var newData = new Array();
    var springFactor = new Number((data.length - 1) / (newLength - 1));
    newData[0] = data[0]; // for new allocation
    for (var i = 1; i < newLength - 1; i++) {
        var tmp = +springFactor * i;
        var before = new Number(Math.floor(tmp)).toFixed();
        var after = new Number(Math.ceil(tmp)).toFixed();
        var atPoint = tmp - +before;
        newData[i] = linearInterpolate(data[before], data[after], atPoint);
    }
    newData[newLength - 1] = data[data.length - 1]; // for new allocation
    return newData;
};

class Colour {

    public IsSet: boolean;
    public RGBString: string;
    public RGBAString: string;
    public HEXString: string;
    public HEXNumber: number;

    public RGBAColourString: string;
    public RGBColourString: string;

    public R: number;
    public G: number;
    public B: number;
    public Alpha: number;

    public FromRGBOneScaleString(rgb: string) {
        var split = rgb.split(" ");
        return this.FromRGBOneScale(+split[0], +split[1], +split[2]);
    }

    public FromRGBOneScale(R: number, G: number, B: number, A?: number) {
        return this.FromRGB(R * 255, G * 255, B * 255, A);
    }

    public FromRGB(R: number, G: number, B: number, A?: number) {

        this.R = R;
        this.G = G;
        this.B = B;

        this.RGBString = R + ',' + G + ',' + B;
        this.RGBAString = R + ',' + G + ',' + B + ',' + ((A != null) ? A : 1);

        this.RGBAColourString = "rgba(" + this.RGBAString + ")";
        this.RGBColourString = "rgb(" + this.RGBString + ")";

        var hex = ("0" + R.toString(16)).slice(-2) + ("0" + G.toString(16)).slice(-2) + ("0" + B.toString(16)).slice(-2);

        this.HEXString = "#" + hex;

        var hexNumber = "0x" + hex;
        this.HEXNumber = +hexNumber;

        this.IsSet = true;

        return this;
    }

    public FromHEX(HEX: string) {
        if (HEX.charAt(0) != "#" || (HEX.length != 4 && HEX.length != 7)) { throw "Not a valid hex string! Expected format '#ABC' or '#AABBCC"; }

        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        HEX = HEX.replace(shorthandRegex, function (m, r, g, b) {
            return r + r + g + g + b + b;
        });

        this.HEXString = "#" + HEX;

        var hexNumber = "0x" + HEX;
        this.HEXNumber = +hexNumber;

        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(HEX);
        var rgb = result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;

        if (!!rgb) {
            this.R = rgb.r;
            this.G = rgb.g;
            this.B = rgb.b;

            this.RGBString = this.R + ',' + this.G + ',' + this.B;
            this.RGBAString = this.R + ',' + this.G + ',' + this.B + ',' + 1;

            this.RGBAColourString = "rgba(" + this.RGBAString + ")";
            this.RGBColourString = "rgb(" + this.RGBString + ")";
        }

        this.IsSet = true;

        return this;
    }
}
